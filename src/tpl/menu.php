<div id="menu">

    <div class="header">
        <h2>
            <a id="title" href="index.php">Sphinxer</a>
            <span id="author">by Robert Jabłecki</span>
        </h2>
    </div>
    <?php if($is_logged){ ?>

        <div>
            <a href="index.php?cmd=query">Zapytanie SQL</a>
            <a href="index.php?cmd=wiki">Wiki</a>
        </div>

        <div class="tables">
            <?php foreach ($tables as $table){ ?>
                <div>
                    <a href="index.php?cmd=browse&table=<?php echo $table ?>"><?php echo $table ?></a>
                </div>
            <?php } ?>
        </div>

    <?php } ?>

</div>
