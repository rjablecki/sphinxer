
<p>
    <?php echo $query ?>
    <span> (<?php echo $meta['time'] ?> s)</span>
</p>

<?php if ($error !== '') { ?>
    <p class="error">
        <?php echo $error ?>
    </p>
    <br>
<?php } ?>

<?php if (is_array($result)){ ?>

    <?php if (count($result) > 0) { ?>

    <table>
        <thead>
        <tr>
            <?php foreach($result[0] as $field => $nil){ ?>
                <th><?php echo $field ?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach($result as $row){ ?>
           <tr>
               <?php foreach($row as $field){ ?>
                   <td><?php echo $field ?></td>
               <?php } ?>
           </tr>
        <?php } ?>
        </tbody>
    </table>

    <br>
    <span>(<?php echo number_format($meta['total_found'], 0, '.', ' ') ?> rekordów)</span>

    <?php } else { ?>
        <?php if ($error === '') { ?>
            <h4>Brak rekordów</h4>
        <?php } ?>
    <?php } ?>

<?php } ?>