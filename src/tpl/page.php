<!doctype html>

<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Sphinxer</title>
    <meta name="author" content="Robert Jabłecki">
    <meta name="description" content="Adminer for Sphinxsearch">
    <meta name="keywords" content="SQL,Sphinx,Index">
    <meta name="robots" content="noindex, nofollow">
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <?php echo $style ?>
</head>

<body>
<div id="content">
    <h2><?php echo $content_title ?></h2>
    <?php echo $content ?>
</div>

<?php echo $menu ?>

</body>
</html>
