<?php
$compars = ['=', '!=', '&lt;', '&gt;', '&lt;=', '&gt;='];

?>
    <p class="links">
        <a href="index.php?cmd=browse&table=<?php echo $table ?>">Pokaż dane</a>
        <a href="index.php?cmd=describe&table=<?php echo $table ?>">Struktura tabeli</a>
    </p>

    <form action="index.php" method="get">
        <input type="hidden" name="cmd" value="browse">
        <input type="hidden" name="table" value="<?php echo $table ?>">

        <fieldset>
            <legend>Szukaj</legend>
            <div id="fieldset-search">
                <?php foreach ($where as $i => $w){ ?>
                    <div>
                        <select name="where[<?php echo $i ?>][col]">
                            <option>
                                <?php foreach ($fields as $field => $type){ ?>
                            <option<?php if ($w['col'] === $field) echo ' selected'?>><?php echo $field ?>
                            <?php } ?>
                        </select>
                        <select name="where[<?php echo $i ?>][op]">
                            <?php foreach ($compars as $c){ ?>
                            <option<?php if ($w['op'] === $c) echo ' selected'?>><?php echo $c ?>
                                <?php } ?>
                        </select>
                        <input type="search" name="where[<?php echo $i ?>][val]" value="<?php echo $w['val'] ?>" >
                    </div>
                <?php } ?>

            </div>
        </fieldset>

        <fieldset>
            <legend>Sortuj</legend>
            <div>
                <select name="order">
                    <option>
                    <?php foreach ($fields as $field => $type){ ?>
                    <option<?php if ($order === $field) echo ' selected' ?>><?php echo $field ?>
                        <?php } ?>
                </select>
                <label>
                    <input type="checkbox" name="desc" value="1"<?php if ($desc) echo ' checked' ?>>
                    malejąco
                </label>
            </div>
        </fieldset>

        <fieldset class="limit">
            <legend>Limit</legend>
            <div>
                <input type="number" name="limit" class="size" value="<?php echo $limit ?>">
            </div>
        </fieldset>

        <fieldset>
            <legend>Opcje</legend>
            <label>
                <input type="checkbox" name="htmlentities" value="1"<?php if ($htmlentities) echo ' checked' ?>>
                htmlentities
            </label>
            <br>
            <label>
                <input type="checkbox" name="json" value="1"<?php if ($json) echo ' checked' ?>>
                json_decode
            </label>
            <br>
        </fieldset>

        <fieldset>
            <legend>Czynność</legend>
            <div>
                <input type="submit" value="pokaż">
            </div>
        </fieldset>

    </form>


    <p><?php echo $query ?>
        <span>(<?php echo $meta['time'] ?> s)</span>
        <a href="index.php?cmd=query&query=<?php echo urlencode($query) ?>">Edytuj</a>
    </p>

<?php if ($error !== '') { ?>
    <p class="error">
        <?php echo $error ?>
    </p>
<?php } ?>

<?php if (is_array($result)){ ?>

    <?php if (count($result) > 0) { ?>

        <table>
            <thead>
            <tr>
                <?php foreach($result[0] as $field => $nil){ ?>
                    <th><?php echo $field ?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach($result as $row){ ?>
                <tr>
                    <?php foreach($row as $field){ ?>
                        <td><?php echo $field ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        
        <br>
        <span>(<?php echo number_format($meta['total_found'], 0, '.', ' ') ?> rekordów)</span>

        <?php echo $pagination ?>

    <?php } else { ?>
        <?php if ($error === '') { ?>
            <h4>Brak rekordów</h4>
        <?php } ?>
    <?php } ?>

    <br>

<?php } ?>