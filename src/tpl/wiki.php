<div>
    SELECT<br>
    select_expr [, select_expr ...]<br>
    FROM index [, index2 ...]<br>
    [WHERE where_condition]<br>
    [GROUP [N] BY {col_name | expr_alias} [, {col_name | expr_alias}]]<br>
    [WITHIN GROUP ORDER BY {col_name | expr_alias} {ASC | DESC}]<br>
    [HAVING having_condition]<br>
    [ORDER BY {col_name | expr_alias} {ASC | DESC} [, ...]]<br>
    [LIMIT [offset,] row_count]<br>
    [OPTION opt_name = opt_value [, ...]]<br>
    [FACET facet_options[ FACET facet_options][ ...]]<br>
</div>
<br>
<div>
    SELECT *, INTERVAL(posted,NOW()-7*86400,NOW()-86400) AS timeseg, WEIGHT() AS w<br>
    FROM example WHERE MATCH('my search query')<br>
    GROUP BY siteid<br>
    WITHIN GROUP ORDER BY w DESC<br>
    ORDER BY timeseg DESC, w DESC<br>
</div>
<br>
<div>
    CALL KEYWORDS ('microsoft ios iphone itunes browser xp nokia' , 'ind_stats', 1)
</div>
<br>
<div>
    SELECT * FROM ind_stats WHERE MATCH ('@v_user_agent -Phone nokia')
</div>

<ul class="itemizedlist" type="disc"><li class="listitem"><p>operator OR: </p><pre class="programlisting">hello | world</pre></li>
    <li class="listitem"><p>operator MAYBE (introduced in verion 2.2.3-beta): </p><pre class="programlisting">hello MAYBE world</pre></li>
    <li class="listitem"><p>operator NOT: <pre class="programlisting">hello -world <br >hello !world</pre></p></li>
    <li class="listitem"><p>field search operator: </p><pre class="programlisting">@title hello @body world</pre></li>
    <li class="listitem"><p>field position limit modifier (introduced in version 0.9.9-rc1): </p><pre class="programlisting">@body[50] hello</pre></li>
    <li class="listitem"><p>multiple-field search operator: </p><pre class="programlisting">@(title,body) hello world</pre></li>
    <li class="listitem"><p>ignore field search operator (will ignore any matches of 'hello world' from field 'title'): </p><pre class="programlisting">@!title hello world</pre></li>
    <li class="listitem"><p>ignore multiple-field search operator (if we have fields title, subject and body then @!(title) is equivalent to @(subject,body)): </p><pre class="programlisting">@!(title,body) hello world</pre></li>
    <li class="listitem"><p>all-field search operator: </p><pre class="programlisting">@* hello</pre></li>
    <li class="listitem"><p>phrase search operator: </p><pre class="programlisting">"hello world"</pre></li>
    <li class="listitem"><p>proximity search operator: </p><pre class="programlisting">"hello world"~10</pre></li>
    <li class="listitem"><p>quorum matching operator: </p><pre class="programlisting">"the world is a wonderful place"/3</pre></li>
    <li class="listitem"><p>strict order operator (aka operator "before"): </p><pre class="programlisting">aaa &lt;&lt; bbb &lt;&lt; ccc</pre></li>
    <li class="listitem"><p>exact form modifier (introduced in version 0.9.9-rc1): </p><pre class="programlisting">raining =cats and =dogs</pre></li>
    <li class="listitem"><p>field-start and field-end modifier (introduced in version 0.9.9-rc2): </p><pre class="programlisting">^hello world$</pre></li>

</ul>