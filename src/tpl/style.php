<style type="text/css">
    @import "//fonts.googleapis.com/css?family=Roboto:400,700";

    body {
        color: #000;
        background: #fff;
        font-family: 'Roboto',Verdana,Arial,Helvetica,sans-serif;
        margin: 0;
        font-size: 14px;
        padding-bottom: 25px;
    }
    * {
        box-sizing: border-box;
    }
    a {
        text-decoration: none;
    }
    a:hover {
        color: red;
    }
    .oh {
        overflow: hidden;
    }
    .col-6 {
        float: left;
        width: 50%;
        min-height: 1px;
    }

    #content {
        margin: 0 0 0 21em;
        padding: 10px 20px 20px 0;
    }
    #menu {
        position: absolute;
        padding: 0 0 30px 0;
        top: 0;
        width: 19em;
    }
    #menu .header {
        padding: 10px 0;
        border-bottom: none;
    }
    #menu .header h2{
        margin: 0;
        background: #eee;
    }
    #menu > div {
        padding: .8em 1em;
        margin: 0;
        border-bottom: 1px solid #ccc;
    }
    #title {
        text-decoration: none;
        color: inherit;
    }
    #author {
        font-size: 11px;
        position: relative;
        top: 1px;
    }
    h2  {
        font-size: 150%;
        margin: 0 0 20px -18px;
        padding: .8em 1em;
        border-bottom: 1px solid #000;
        color: #000;
        font-weight: normal;
        background: #ddf;
    }
    table {
        border-collapse: collapse;
        margin: 1em 20px 0 0;
        border: 0;
        border-top: 1px solid #999;
        border-left: 1px solid #999;
        font-size: 90%;
    }
    thead td, thead th {
        background: #ddf;
    }
    td, th {
        border: 0;
        border-right: 1px solid #999;
        border-bottom: 1px solid #999;
        padding: .2em .3em;
    }
    th {
        background: #eee;
        text-align: left;
    }
    textarea {
        font-family: monospace;
        width: 100%;
        resize: vertical;
    }
    .error, .message {
        padding: .5em .8em;
        margin: 1em 20px 0 0;
    }
    .error {
        color: red;
        background: #fee;
    }
    .message {
        color: green;
        background: #efe;
    }
    fieldset {
        display: inline;
        vertical-align: top;
        padding: .5em .8em;
        margin: .8em .5em 0 0;
        border: 1px solid #999;
    }
    input[type="search"]{
        height: 19px;
    }
    input[type="checkbox"] {
        position: relative;
        top: 2px;
    }
    input[type="number"]{
        width: 55px;
    }
    #fieldset-search > div{
        margin-bottom: 2px;
    }
    .pagination {
        position: fixed;
        bottom: 0;
        left: 21em;
        padding: 5px;
        background: #ddf;
        border: 1px solid #999;
    }
    .pagination ul{
        margin: 0;
        padding: 0;
    }
    .pagination li{
        list-style-type: none;
        display: inline-block;
        margin: 0 3px;
    }
    .pagination .pure-button-active {
        color: red;
    }


    .oops {
        font-family: inherit;
        font-size: 64px;
        text-align: center;
    }
    .oops-text {
        font-size: 20px;
        font-weight: 400;
        text-align: center;
    }

</style>