<?php

namespace SphinxTool;

/**
 * The singleton trait.
 *
 * @see http://en.wikipedia.org/wiki/Singleton_pattern
 * @since 1.0.0
 */
trait SingletonTrait
{
    /** @var static The instance. */
    private static $instance;

    /**
     * Protected constructor to prevent creating a new instance of the Singleton class from outside the object.
     */
    protected function __construct() { }

    /**
     * Protected clone method to prevent cloning of the Singleton instance.
     */
    protected function __clone() { }

    /**
     * Returns the only instance of the Singleton class.
     *
     * @return static the only instance of the Singleton class.
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }
}