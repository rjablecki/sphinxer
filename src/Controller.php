<?php

namespace SphinxTool;

use Exception;

class Controller
{
    /**
     * Current table name
     * @var
     */
    private $table;

    /**
     * @var Sphinx instance
     */
    private $sphinx;

    /**
     * @var string page content
     */
    private $content = '';

    /**
     * @var string page title
     */
    private $title = 'Index';

    /**
     * @var string error message
     */
    private $error = '';

    private $isLogged = false;

    public function __construct()
    {
        $this->auth = new Auth();

        if ($this->auth->isLogged()) {
            $this->sphinx = new Sphinx();
            $this->handleTable();
            $this->route();
        } else {
            $this->actionLogging();
        }

        $this->render();
    }

    private function handleTable()
    {
        $tables = $this->sphinx->getTables();
        $table = Form::get('table');

        if (!is_null($table) && !in_array($table, $tables)){
            $this->actionError();
        }
        $this->table = $table;
    }

    private function route()
    {
        $method = Form::get('cmd', 'string', 'index');
        $method = 'action'.strtolower(ucfirst($method));

        if (!is_callable([$this, $method])){
            $method = 'actionError';
        }
        call_user_func_array([$this, $method],[]);
    }

    private function render()
    {
        $isLogged = $this->auth->isLogged();

        if ($isLogged) {
            $tables = $this->sphinx->getTables();
        }  else {
            $tables = [];
        }

        $menu = (new View)->with('tables', $tables)->with('is_logged', $isLogged)->getHTML('menu');

        $style = $this->getStyle();

        (new View)
            ->with('menu', $menu)
            ->with('style', $style)
            ->with('content', $this->content)
            ->with('content_title', $this->title)
            ->parse('page');

        $this->end();
    }

    private function getStyle()
    {
        $style = (new View)->getHTML('style');
        $style = str_replace(["\n", "        ", "\t",'    '], '', $style);
        $style = str_replace([' {', ': '], ['{', ':'], $style);
        return $style;
    }

    private function actionBrowse()
    {
        $where = Form::get('where', 'array', []);
        $limit = Form::get('limit', 'int', 50);
        $order = Form::get('order', 'string', '');
        $page = Form::get('p', 'int', 1);
        $desc = (bool)Form::get('desc', 'int', 0);
        $htmlentities = (bool)Form::get('htmlentities', 'int', 0);
        $json = (bool)Form::get('json', 'int', 0);

        $table = $this->table;
        $fields = $this->sphinx->getFields($table);

        $query = 'SELECT * FROM '.$table;

        if (count($where) > 0){
            $conds = [];
            foreach($where as $key => $cond){
                if (
                    strlen($cond['col']) > 0 &&
                    strlen($cond['val']) > 0 &&
                    strlen($cond['op']) > 0
                ){
                    $field = $cond['col'];
                    $val = in_array($fields[$field], ['bigint', 'uint', 'timestamp']) ? $cond['val'] : Db::sql($cond['val']);
                    $conds[] = ' '.$field.' '.$cond['op'].' '.$val;
                } else {
                    unset($where[$key]);
                }
            }
            if (count($conds) > 0){
                $query .= ' WHERE '.implode(' AND ', $conds);
            }
        }
        $where[] = [
            'col' => '',
            'op' => '=',
            'val' => '',
        ];

        if ($order !== ''){
            $query .= ' ORDER BY '.$order.' ';
            $query .= $desc ? 'DESC' : 'ASC';
        }

        $options = '';
        if ($limit > 0){
            $query .= ' LIMIT ';
            if ($page > 1){
                $query .= ($page * $limit - $limit).',';
            }
            $query .= $limit;
            $options .= ' option max_matches='.($page*$limit);
        }

        try {
            $result = $this->sphinx->query($query.$options);
        } catch (Exception $e){
            $result = [];
            $this->error = $e->getMessage();
        }

        $meta = $this->sphinx->meta();

        $pagination = $this->getPagination($meta['total_found'], $limit);

        $this->content .= (new View)
            ->with('result', $result)
            ->with('table', $table)
            ->with('query', $query)
            ->with('limit', $limit)
            ->with('where', $where)
            ->with('order', $order)
            ->with('desc', $desc)
            ->with('htmlentities', $htmlentities)
            ->with('json', $json)
            ->with('fields', $fields)
            ->with('meta', $meta)
            ->with('error', $this->error)
            ->with('pagination', $pagination)
            ->getHTML('browse');

        $this->title = 'Pokaż: '.$table;
    }

    private function getPagination($total, $limit)
    {
        $pagination = '';
        if ($total > $limit){
            $pagination = (new Pagination)
                ->total($total)
                ->per_page($limit)
                ->page_name('p')
                ->link_active_class('pure-button-active')
                ->paginate();
        }

        return $pagination;
    }

    private function actionIndex()
    {
        // $this->content .= (new View())->getHTML('wiki');
        // $this->title = 'Dokumentacja';
    }

    private function actionLogging()
    {
        $default = [
            'host' => '127.0.0.1',
            'user' => '',
            'password' => '',
            'port' => '9306',
        ];
        $data = array_merge($default, $_POST);

        $this->content .= (new View)->with($data)->getHTML('logging');
        // $this->title = 'Dokumentacja';
    }

    private function actionWiki()
    {
        $this->content .= (new View)->getHTML('wiki');
        $this->title = 'Dokumentacja';
    }

    private function actionQuery()
    {
        $query = Form::post('query', 'string', '');

        if ($query !== ''){
            try {
                $result = $this->sphinx->query($query);
            } catch (Exception $e){
                $result = [];
                $this->error = $e->getMessage();
            }
            $meta = $this->sphinx->meta();
            $this->content .= (new View)
                ->with('result', $result)
                ->with('error', $this->error)
                ->with('meta', $meta)
                ->with('query', $query)
                ->getHTML('query_result');
        }

        if ($query === ''){
            $query = isset($_GET['query']) ? $_GET['query'] : '';
        }

        $this->content .= (new View)
            ->with('query', $query)
            ->getHTML('query');

        $this->title = 'Zapytanie SQL';
    }

    private function actionDescribe()
    {
        $table = $this->table;
        $fields = $this->sphinx->getFields($table);
        $this->content = (new View)
            ->with('fields', $fields)
            ->with('table', $table)
            ->getHTML('describe');

        $this->title = 'Tabela: '.$table;
    }

    private function actionError($message = '')
    {
        $this->title = 'Bład';
        $this->content .= (new View)
            ->with('message', $message)
            ->getHTML('error_page');
        $this->render();

    }

    private function end()
    {
        die;
    }
}