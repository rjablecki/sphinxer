<?php

namespace SphinxTool;

class View {

    private $data = [];

    public function with($key, $value = null)
    {
        if (is_array($key)){
            $this->data = array_merge($this->data, $key);
        } else {
            $this->data[$key] = $value;
        }
        
        return $this;
    }

    public function getHTML($name)
    {
        $path = PATH_TPL.$name.'.php';

        if (!file_exists($path)){
            die('nie ma templatki '.$path);
        }
        ob_start();
        extract($this->data);
        require $path;

        return ob_get_clean();
    }

    public function parse($name)
    {
        echo $this->getHTML($name);
    }
}