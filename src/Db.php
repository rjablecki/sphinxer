<?php

namespace SphinxTool;

use mysqli, mysqli_result, Exception;

class Db
{
    use SingletonTrait;

    /**
     * @var mysqli
     */
    private $db;

    public function connect($host, $user, $password, $port)
    {
        $this->db = new mysqli($host, $user, $password, '', $port);
    }

    public function getRows($sql)
    {
        return $this->collect($this->query($sql));
    }

    public function getRow($sql)
    {
        $result = $this->getRows($sql);

        if (is_array($result) && count($result) > 0){
            $result = array_shift($result);
        }

        return $result;
    }

    public function exec($sql)
    {
        $result = $this->db->query($sql);

        if ($result === false) {
            $error = $this->db->error;
            throw new Exception(sprintf('"%s" sql: "%s"', $error, $sql));
        }

        return $result;
    }

    public function insert($sql)
    {
        if ($this->exec($sql)){
            return mysqli_insert_id($this->db);
        }
        return false;
    }

    private function query($sql)
    {
        $result = $this->db->query($sql);

        if (!$result instanceof mysqli_result) {
            throw new Exception(sprintf('"%s" sql: "%s"', $this->db->error, $sql));
        }

        return $result;
    }

    private function collect(mysqli_result $result)
    {
        $rows = [];
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function getConnection()
    {
        return $this->db;
    }

    public static function sql($var, $type = 'string')
    {
        if ($type === 'int'){
            return (int)$var;
        } elseif ($type === 'float'){
            return (float)$var;
        }
        $mysqli = Db::getInstance()->getConnection();

        return sprintf("'%s'", mysqli_real_escape_string($mysqli, $var));
    }

}


