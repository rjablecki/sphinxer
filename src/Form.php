<?php

namespace SphinxTool;

class Form
{
    public static function post($key, $type = 'string', $default = null)
    {
        return self::cast($_POST, $key, $type, $default);
    }

    public static function get($key, $type = 'string', $default = null)
    {
        return self::cast($_GET, $key, $type, $default);
    }

    private static function cast(array $source, $name, $type, $default)
    {
        $val = $default;
        if (array_key_exists($name, $source)){
            $var = $source[$name];
            if ($type === 'string') {
                $val = (string)$var;
            } else if ($type === 'int') {
                $val = (int)$var;
            } else if ($type === 'array') {
                $val = $var;
            } else {
                throw new \Exception('Invalid type');
            }
        }
        return $val;
    }

}
