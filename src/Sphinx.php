<?php

namespace SphinxTool;

class Sphinx
{
    private $db;

    private $tables = [];

    public function __construct()
    {
        $host = $_SESSION['host'];
        $user = $_SESSION['user'];
        $password = $_SESSION['password'];
        $port = $_SESSION['port'];

        $this->db = Db::getInstance();
        $this->db->connect($host, $user, $password, $port);
        $this->tables = $this->getTables();
    }

    public function getTables()
    {
        $tables = [];
        $date = $this->db->getRows('show tables');
        foreach ($date as $row) {
            $tables[] = $row['Index'];
        }

        return $tables;
    }

    public function getFields($indexName)
    {
        $fields = [];
        $date = $this->db->getRows('describe '.$indexName);
        foreach ($date as $row) {
            $fields[$row['Field']] = $row['Type'];
        }

        return $fields;
    }

    public function query($query)
    {
        return $this->db->getRows($query);
    }

    public function meta()
    {
        $res = $this->query('SHOW META');
        $meta = [];
        foreach ($res as $row) {
            $meta[$row['Variable_name']] = $row['Value'];
        }

        return $meta;
    }
}