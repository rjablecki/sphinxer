<?php
namespace SphinxTool;

class Auth
{
    private $isLogged = false;

    public $data = [
        'host' => false,
        'user' => false,
        'password' => false,
        'port' => false,
    ];

    public function __construct()
    {
        foreach ($this->data as $var => $n) {
            $send = Form::post($var);
            if (!is_null($send)) {
                $_SESSION[$var] = $send;
            }
        }
        $this->handleSession();
    }

    public function isLogged()
    {
        return $this->isLogged;
    }

    private function handleSession()
    {
        $session = $_SESSION;

        foreach ($this->data as $var => $n) {
            if (isset($session[$var])) {
                $this->data[$var] = $session[$var];
            }
        }
        $this->isLogged = !in_array(false, $this->data, true);
    }

}