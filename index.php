<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('SRC', __DIR__.'/src/');
define('PATH_TPL', SRC.'tpl/');

session_start();

require_once SRC.'SingletonTrait.php';
require_once SRC.'Form.php';
require_once SRC.'Db.php';
require_once SRC.'Auth.php';
require_once SRC.'Sphinx.php';
require_once SRC.'View.php';
require_once SRC.'Controller.php';
require_once SRC.'Pagination.php';

new \SphinxTool\Controller;
